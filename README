This repository contains the inlists used to generate the
MESA presupernova progenitors presented in 

Fernandez et al. (2018), MNRAS, 476, 2366

The inlists are for MESA version 6794 (MESA SDK 
version 20141212 works smoothly for compiling on
Linux machines) and are based on the inlists
published in Fuller et al. (2015; see reference below).

Each inlist is labelled after the ZAMS mass
and the initial metallicity used, so for example

inlist_M12_z-4

corresponds to a 12Msun model with Z = 1e-2Msun = 2e-4

Models can take anywhere from 10min to about 1hr
to complete using 8 OpenMP threads.

If using these inlist files for publishable work, 
please reference the following papers:

1) Fernandez et al. (2018), MNRAS, 476, 2366
2) Fuller et al. (2015), ApJ, 810, 101
3) Paxton et al. (2011), ApJS, 192, 3
4) Paxton et al. (2013), ApJS, 208, 4
5) Paxton et al. (2015), ApJS, 220, 15

